<h1>Preprocessing</h1>
Create file config.yaml in the project directory like:

```YAML
url: API_URL
headers:
  accept: 'application/json'
  content-type: 'application/json'
  Authorization: YOUR_TOKEN
```


<h1> Test project </h1>

<ul>
<li>
<h2>ClientView:</h2>
url: http://127.0.0.1:8000/client/

Data example:
```JSON
{
    "id": 1,
    "phone":"79181955710",
    "operator_code": "918",
    "tag": "AAA",
    "tz":"UTC+3"
}
```
<h3>Method get:</h3>
Returns list of clients, does not requires clients data

Response body: 
```JSON
{
    "clients": [
        {
            "id": 1,
            "phone": "89780014559",
            "operator_code": "918",
            "tag": "AAA",
            "tz": "UTC+3"
        },
        ...
    ]
}
```
<h3>Method post:</h3>
Adds a new client

Response body: Successful

<h3>Method delete:</h3>
Delete client

Response body: Successful

<h3>Method put:</h3>
Update client

Response body: Successful
</li>

<li>
<h2>MailingView:</h2>
url: http://127.0.0.1:8000/mail/

Data example:
```JSON
{
    "id": 1,
    "start_date":"2022-08-23 16:03:00",
    "mail": "hello",
    "filter": "AAA",
    "end_date": "2022-08-23 16:10:01"
}
```
<h3>Method get:</h3>
Returns info about mailings, does not requires mailing data

Response body: 
```JSON 
{
    "mailing 1": [],
    "mailing 2": [
        {
            "state": "200",
            "cnt": 2
        }
    ],
    "mailing 3": [
        {
            "state": "200",
            "cnt": 2
        }
    ]
}
```
<h3>Method post:</h3>
Adds a new mailing and starts a waiting thread

Response body: Successful

<h3>Method delete:</h3>
Delete mailing

Response body: Successful

<h3>Method put:</h3>
Update mailing

Response body: Successful
</li>

<li>
<h2>MailingDetailView:</h2>
url: http://127.0.0.1:8000/mail/{mailing_id}

Data example: 
```JSON
{
    "id": 1,
    "start_date":"2022-08-23 16:03:00",
    "mail": "hello",
    "filter": "AAA",
    "end_date": "2022-08-23 16:10:01"
}
```
<h3>Method get:</h3>
Returns info about mailings, requires mailing data

Response body:
```JSON
{
    "id": 2,
    "start_date": "2022-08-23 15:13:43+00:00",
    "mail": "AAA",
    "filter": "AAA",
    "end_date": "2022-08-23 23:13:50+00:00",
    "messages": [
        {
            "id": 31,
            "send_date": "2022-08-23 18:38:43.998350+00:00",
            "state": "200",
            "mailing": 2,
            "client": 2
        },
        {
            "id": 32,
            "send_date": "2022-08-23 18:38:44.278697+00:00",
            "state": "200",
            "mailing": 2,
            "client": 3
        }
    ]
}
```
</li>
</ul>

