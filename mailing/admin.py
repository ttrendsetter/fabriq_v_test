from django.contrib import admin

from . import models


# Creation an admin's menu

class MessageInline(admin.TabularInline):
    model = models.Message


class ClientAdmin(admin.ModelAdmin):
    inlines = [
        MessageInline
    ]


class MailingAdmin(admin.ModelAdmin):
    inlines = [
        MessageInline
    ]


admin.site.register(models.Mailing, MailingAdmin)
admin.site.register(models.Client, ClientAdmin)
admin.site.register(models.Message)
