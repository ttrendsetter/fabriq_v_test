import json

from django.db.models import Count
from django.http.response import HttpResponse, JsonResponse
from django.http.response import HttpResponseForbidden
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from .main import start_thread
from .models import Mailing, Client


# Create your views here.
@method_decorator(csrf_exempt, name='dispatch')
class ClientView(View):
    """
        CRUD for client
    """
    model = Client

    @staticmethod
    def post(request):
        # method of adding a new client
        data = json.loads(request.body)
        client = Client(**data)
        try:
            client.validate_unique()
        except:
            return HttpResponseForbidden("""Client exists""")

        client.save()
        return HttpResponse('Successful')

    @staticmethod
    def put(request):
        # method for changing client data
        data = json.loads(request.body)
        try:
            client = Client.objects.get(id=data['id'])
            client.phone = client.phone if data['phone'] is None else data['phone']
            client.operator_code = \
                (client.operator_code if data['operator_code'] is None else data['operator_code'])
            client.tag = client.tag if data['tag'] is None else data['tag']
            client.tz = client.tz if data['tz'] is None else data['tz']
            client.save()
        except:
            return HttpResponseForbidden('The client does not exist')
        return HttpResponse('Successful')

    @staticmethod
    def delete(request):
        # method for deleting a client
        data = json.loads(request.body)
        try:
            print(data['id'])
            client = Client.objects.get(id=data['id'])
            client.delete()
        except:
            return HttpResponseForbidden('The client does not exist')
        return HttpResponse('Successful')

    @staticmethod
    def get(request):
        # method for data collection
        clients = Client.objects.all()
        res = []
        for client in clients:
            client_data = {
                'id': client.id,
                'phone': client.phone,
                'operator_code': client.operator_code,
                'tag': client.tag,
                'tz': client.tz
            }
            res.append(client_data)
        res = {'clients': res}
        return JsonResponse(res)


@method_decorator(csrf_exempt, name='dispatch')
class MailingView(View):
    """
        CRUD for Mailings
    """

    def post(self, request):
        # creating new mailing
        data = json.loads(request.body)
        mailing = Mailing(**data)
        try:
            mailing.validate_unique()
        except:
            return HttpResponseForbidden("""Mailing exists""")

        mailing.save()
        start_thread(mailing)
        return HttpResponse('Successful')

    def put(self, request):
        # update handler
        data = json.loads(request.body)
        try:
            mailing = Mailing.objects.get(id=data['id'])
            mailing.start_date = \
                (mailing.start_date if data['start_date'] is None else data['start_date'])
            mailing.mail = \
                (mailing.mail if data['mail'] is None else data['mail'])
            mailing.filter = \
                (mailing.filter if data['filter'] is None else data['filter'])
            mailing.end_date = \
                (mailing.end_date if data['end_date'] is None else data['end_date'])
            mailing.save()
            start_thread(mailing)
        except:
            return HttpResponseForbidden('The mailing does not exist')
        return HttpResponse('Successful')

    def delete(self, request):
        # method for deleting a mailing
        data = json.loads(request.body)
        try:
            mailing = Mailing.objects.get(id=data['id'])
            mailing.delete()
        except:
            HttpResponseForbidden('The mailing does not exist')
        return HttpResponse('Successful')

    def get(self, request):
        # a method for collecting and processing mailing data
        mailings = Mailing.objects.all()
        res = ({'mailing ' + str(mailing.id): (
            list(mailing.message_set.values('state').annotate(cnt=Count('state'))))
            for mailing in mailings})

        return JsonResponse(res)


class MailingDetailView(View):
    """
        Detail mailing info
    """

    def get(self, request, id):
        mailing = Mailing.objects.get(id=id)
        messages_list = mailing.message_set.all()
        name_list = ['id', 'send_date', 'state', 'mailing', 'client']
        values_list = messages_list.values_list()
        messages = []
        for values in values_list:
            val = list(values)
            val[1] = str(val[1])
            messages.append(dict(zip(name_list, val)))
        result = {
            'id': mailing.id,
            'start_date': str(mailing.start_date),
            'mail': mailing.filter,
            'filter': mailing.filter,
            'end_date': str(mailing.end_date),
            'messages': messages
        }
        return JsonResponse(result)
