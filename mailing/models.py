from django.db import models
from django.urls import reverse


# Create your models here.


class Client(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    phone = models.CharField(max_length=11)
    operator_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=3)
    tz = models.CharField(max_length=9)

    def __str__(self):
        return str(self.id)


class Mailing(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    start_date = models.DateTimeField()
    mail = models.TextField()
    filter = models.CharField(max_length=3)
    end_date = models.DateTimeField()

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        reverse('mailing_detail', kwargs={'id': str(self.id)})


class Message(models.Model):
    send_date = models.DateTimeField(null=True)
    state = models.CharField(max_length=10)
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return str(self.pk)
