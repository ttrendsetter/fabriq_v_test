import datetime

import yaml

from mailing_proj.settings import BASE_DIR


def read_config():
    try:
        with open(f'{BASE_DIR}/config.yaml', 'r') as configs:
            data = yaml.safe_load(configs)
        url = data['url']
        headers = data['headers']
        return url, headers
    except FileNotFoundError:
        raise Exception('Please create file config.yaml')


def convert_date(date):
    return datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S').timestamp()
