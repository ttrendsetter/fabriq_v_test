from django.urls import path

from . import views

urlpatterns = [
    path('client/', views.ClientView.as_view(), name='client'),
    path('mail/', views.MailingView.as_view(), name='mailing'),
    path('mail/<int:id>', views.MailingDetailView.as_view(), name='mailing_detail')
]
