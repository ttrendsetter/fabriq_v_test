import asyncio
import datetime
import json
import threading
import time

import aiohttp
from django.db.models import Q

from .models import Message, Mailing, Client
from .utils import convert_date
from mailing import URL, HEADERS



def start_thread(mailing: Mailing):
    thread = threading.Thread(target=start_async, args=[mailing])
    thread.start()


def start_async(mailing: Mailing):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(waiting(mailing))


async def waiting(mailing: Mailing):
    start = convert_date(mailing.start_date)
    end = convert_date(mailing.end_date)
    while True:
        now = datetime.datetime.now().timestamp()
        if start <= now < end:
            await start_mailing(mailing)
            return
        if end <= now:
            return
        time.sleep(1)


async def mailer(mailing: Mailing, message: Message, session, msg):
    end = convert_date(mailing.end_date)
    now = datetime.datetime.now()
    if now.timestamp() < end:
        async with session.post(url=f'{URL}{message.pk}', headers=HEADERS, data=msg) as response:
            message.send_date = now
            message.state = response.status

    message.save() if message.send_date else message.delete()


async def start_mailing(mailing: Mailing):
    filter_ = mailing.filter

    clients = Client.objects.filter(Q(tag=filter_) | Q(operator_code=filter_)).all()
    messages = ([Message(mailing=mailing, client=client)
                 for client in clients])
    tasks = []
    async with aiohttp.ClientSession() as session:
        for message in messages:
            id_ = message.pk
            msg = {
                'id': id_,
                'phone': message.client.phone,
                'text': mailing.mail
            }
            msg = json.dumps(msg)
            task = mailer(mailing, message, session, msg)
            tasks.append(task)

        await asyncio.gather(*tasks)
